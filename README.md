<div align="center">
  <img src="https://seimpairiyun.thedev.id/img/lpse2e.png" width="150px" alt="Logo">
</div>

---

<div align="center">
  <img src="https://img.shields.io/badge/Python-3.x-yellow.svg">
  <img src="https://img.shields.io/badge/Pyqt5-5.15.1-green.svg">
  <img src="https://img.shields.io/badge/License-MIT-red.svg">
  <img src="https://img.shields.io/badge/Tools-scrapping-blue"> 
</div>

# LPSE 2E

LPSE Two Engine is an open-source tool that automates the process of downloading LPSE data.
This tool is built with <b>PyProc</b> as its main engine, while the other engine uses <b>Serebs4</b> (Selenium, Requests, and BeautifulSoup4). The file exported from this tool is in Excel format. Enjoy!

## Screenshots

<div align="center">
  <img style="max-width:800px;width:100%;height:auto;" src="https://i.ibb.co/1dmgcQ1/lpse-starting.png" alt="app starting">
  <img style="max-width:800px;width:100%;height:auto;" src="https://s10.gifyu.com/images/lpse-running.gif" alt="app running">
</div>

## Dependency

```
- Windows 64 bit
- Latest Google Chrome
```
