from requests import get, exceptions as exReq
from urllib3 import disable_warnings, exceptions as exUrl
from socket import gethostbyname
from re import sub, findall
from csv import reader

disable_warnings(category=(exUrl.InsecureRequestWarning,))


def isLinkUp(url):
    reHttp = r"https?://"
    reBlock = r"Forbidden|Block"
    host = sub(reHttp, "", url)

    try:
        # User https protocol
        req = get(url, timeout=5)
        if findall(reBlock, req.reason):
            try:
                get(url, timeout=5, verify=False)
                status = "Up, use http"
            except:
                # Blocked by AV
                status = "Blocked"
        else:
            status = "Up, use https"
    except exReq.SSLError:
        # Use http protocol
        try:
            get(url, timeout=5, verify=False)
            status = "Up, use http"
        except:
            # Blocked by AV
            status = "Blocked"
    except exReq.ConnectionError:
        try:
            # Use direct IP
            ip = f"http://{gethostbyname(host)}"
            get(ip, timeout=5)
            status = "Up, use http and IPv4"
        except:
            status = "Error"
    except exReq.ReadTimeout:
        status = "Timeout"
    except Exception as e:
        status = e

    return f"{url} | {status}"


#csvFile = "data/link-lpse-nasional.csv"
csvFile = "data/link-lpse-aceh.csv"
with open(csvFile, "r") as file:
    csv = reader(file, delimiter=";")
    for row in csv:
        nama, link = row
        # break
        print(f"{nama} | {isLinkUp(link)}")

# test = isLinkUp("http://lpse.konkepkab.go.id")
# print(test)

# u = "http://lpse.konawekab.go.id"
# test = get(uu, timeout=5)
# print(test.reason)
